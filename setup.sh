# install rclone
# ref: https://rclone.org/downloads/
curl https://rclone.org/install.sh | sudo bash

# install bitwarden
# ref: https://bitwarden.com/help/cli/
wget https://github.com/bitwarden/cli/releases/download/v1.22.1/bw-linux-1.22.1.zip -O ~/Downloads/bw-linux-1.22.1.zip
unzip ~/Downloads/bw-linux-1.22.1.zip -d ~/Downloads
sudo install ~/Downloads/bw /usr/local/bin
rm ~/Downloads/bw-linux-1.22.1.zip
bw login # will need email, password, API key, 2FA.
echo "alias bwu='bw unlock'" >> ~/.bashrc
echo "alias bwg='bw get password'" >> ~/.bashrc

# install protonvpn
wget https://protonvpn.com/download/protonvpn-stable-release_1.0.1-1_all.deb -O ~/Downloads/protonvpn-stable-release_1.0.1-1_all.deb
sudo apt install ~/Downloads/protonvpn-stable-release_1.0.1-1_all.deb
sudo apt update
sudo apt install protonvpn-cli --y
protonvpn-cli login
echo "alias vpn='protonvpn-cli connect -f'" >> ~/.bashrc
echo "alias vpnd='protonvpn-cli disconnect'" >> ~/.bashrc
echo "alias vpns='protonvpn-cli status'" >> ~/.bashrc
vpn

# install R and RStudio
# sudo apt install gdebi-core
# export R_VERSION=4.2.0
# curl -O https://cdn.rstudio.com/r/ubuntu-2204/pkgs/r-${R_VERSION}_1_amd64.deb
# sudo gdebi r-${R_VERSION}_1_amd64.deb
# rm r-${R_VERSION}_1_amd64.deb
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo gpg --dearmor -o /usr/share/keyrings/r-project.gpg
echo "deb [signed-by=/usr/share/keyrings/r-project.gpg] https://cloud.r-project.org/bin/linux/ubuntu jammy-cran40/" | sudo tee -a /etc/apt/sources.list.d/r-project.list
sudo apt update
sudo apt install --no-install-recommends r-base


curl -O https://download1.rstudio.org/desktop/bionic/amd64/rstudio-2022.02.2-485-amd64.deb
sudo gdebi rstudio-2022.02.2-485-amd64.deb
rm rstudio-2022.02.2-485-amd64.deb
